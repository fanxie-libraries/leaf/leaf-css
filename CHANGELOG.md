# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Add
- Tabs
- Tooltip
- Modal
- Complex Pagination
- Breadcrumbs

## [[0.6.8](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.6.8)] - 2018-12-30
### Build
- Updated build tools and dependencies.

### Fixed
- Fixed a built issue caused by missing semicolons throughout the entire project.

## [[0.6.7](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.6.7)] - 2018-11-23
### Fixed
- Fixed multiple large select padding.

## [[0.6.6](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.6.6)] - 2018-11-23
### Fixed
- Fixed large select padding.

## [[0.6.5](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.6.5)] - 2018-11-23
### Fixed
- Fixed form elements border color.

## [[0.6.4](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.6.4)] - 2018-11-16
### Fixed
- Added missing styles for the `hr` element.
- Fixed an issue with columns in mobile.

## [[0.6.3](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.6.3)] - 2018-10-25
### Fixed
- Fixed `code` elements background and text colors.

## [[0.6.2](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.6.2)] - 2018-10-25
### Fixed
- Fixed `code` elements background and text colors.

## [[0.6.1](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.6.1)] - 2018-10-13
### Fixed
- Fixed missing styles for `is-fixed-top` and `is-fixed-bottom` navbar modifiers.

## [[0.6.0](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.6.0)] - 2018-10-13
### Added
- Added `content` styles.
- Added `is-loading` modifier to form elements.

## [[0.5.2](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.5.2)] - 2018-10-09
### Fixed
- Fixed navbar dropdown z-index.
- Fixed navbar item bottom padding.

## [[0.5.1](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.5.1)] - 2018-10-09
### Added
- Added `navbar` "`is-shadowed`" modifier.

### Fixed
- Fixed navbar brand font size.

## [[0.5.0](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.5.0)] - 2018-10-07
### Added
- Added form styles for:
    - Input
    - Textarea
    - Select
    - Checkbox
    - Radio
    - Help texts

## [[0.4.1](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.4.1)] - 2018-10-07
### Changed
- Multiple color adjustments.

## [[0.4.0](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.4.0)] - 2018-10-02
### Added
- Notifications
- Simple pagination
- **Build:** `npm run prod` now builds 2 separate bundles, an opinionated one (loading Roboto font from Google Fonts) and another one with generic fonts.

    So, `css/leaf.css` will use default fonts and `css/leaf_roboto.css` will load Roboto.

    This **only** affects the production build, Roboto will always be loaded when using `npm run dev` or `npm run watch`.

## [[0.3.1](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.3.1)] - 2018-10-02
### Changed
- Table styles are now complete.

## [[0.3.0](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.3.0)] - 2018-10-01
### Added
- Buttons
- Tags
- Cards
- Main menu - Top
- Main menu - Sidebar
- Basic section
- Basic table
- Icon base styles


## [[0.2.1](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.2.1)]
### Changed
- Legacy Grid system adjusted to meet library requirements.
- Final colors set.

### Removed
- Removed legacy styles.

## [[0.1.1](https://gitlab.com/fanxie-libraries/leaf-css/tags/v0.1.1)]

### Added
- Legacy styles.
- Created basic Flex-based grid.